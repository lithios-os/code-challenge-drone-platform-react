
# React Web - Coding Challenge

Lithios is looking for people who can build awesome products for our clients, so we created this challenge to test our candidates' overall creativity and developer skills.

## Instructions

#### 1. Build your app

You have 1 week to complete the challenge.

*Implementation and design will be evaluated.*


#### 2. Submit your challenge

Follow these instructions to submit your challenge.

* Fork the Challenge Repository
* Setup your Development Environment ([React - Getting Started guide](https://reactjs.org/docs/getting-started.html))
* Write your Code
* Commit your Changes
* Notify the Lithios Team


#### 3. Impress us with your skills

## Challenge

You are working with a client who designs and manufactures drones for ariel drone light shows, and would like to be able to control the drones from a new React web dashboard. You are tasked with building the first few components of the app.

## Requirements

Your app should be able to complete the following tasks:

* View list of available drones and brief details (name, UUID, battery level, etc.)
* Add a new drone
* Remove drones
* View status of ariel show
* Start ariel show
* End ariel show
* Persist data using Redux

## Grading

The grading of the app will be based off of three criteria:

* **30%** - UI and UX
* **40%** - Overall Design and Structure
* **30%** - Data Management and Store
